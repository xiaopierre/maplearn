# -*- coding: utf-8 -*-
"""
Script d'utilisation de Benchmark Classification, à partir du fichier de
configuration (config.cfg)

Created on Tue Jul  7 14:22:14 2015

@author: thomas_a
"""
from __future__ import print_function
import os
import sys
import getopt
from datetime import datetime
#import logging.config

from maplearn.app.benchmark import Benchmark
from maplearn.app.config import Config
from maplearn.app.reporting import ReportWriter

def main(argv):
    """
    Exécution d'un traitement à partir d'un fichier de configuration
    """
    configfile = 'config.cfg'
    try:
        opts, _ = getopt.getopt(argv, "hc:", ["configfile=", ])
    except getopt.GetoptError:
        print('run.py -c <configfile>')
        sys.exit(2)
    for opt, arg in opts:
        if opt == '-h':
            print('run.py -c <configfile>')
            sys.exit()
        elif opt in ("-c", "--configfile"):
            configfile = arg
    print('Config file : %s', configfile)

    # chargement de la configuration
    cfg = Config(configfile)
    #logging.config.fileConfig(cfg.dirs['bin'], 'logging.cfg')
    # redirection des print vers 1 fichier
    report_file = os.path.join(cfg.dirs['sortie'], 'rapport_benchmark')
    report_writer = ReportWriter(report_file)
    sys.stdout = report_writer

    print('#MAPPING LEARNING (%s)#' % datetime.today().strftime('%d/%m/%Y'))
    print(cfg)

    ben = Benchmark(cfg.dirs['sortie'],
                    type=cfg.process['type'],
                    algorithm=cfg.process['algorithm'],
                    metric=cfg.process['distance'],
                    kfold=cfg.process['kfold'],
                    codes=cfg.entree['codes'])
    
    #TODO: PATCH tout moche à retirer dés que possible
    if cfg.process['type'] == 'clustering' and cfg.entree['echantillons'] is None:
        cfg.entree['echantillons'] = [cfg.entree['data'], ]

    for _file in cfg.entree['echantillons']:
        __params = {i:cfg.entree[i] for i in ('classe', 'classe_id',
                                              'features', 'na')}
        ben.load(_file, **__params)
        if not cfg.entree['data'] is None:
            ben.load_data(cfg.entree['data'], features=cfg.entree['features'])
        basename = os.path.splitext(os.path.basename(_file))[0]
        ben.dataset.plot(os.path.join(cfg.dirs['sortie'],
                                      'sig_%s.png' % basename))
        print(ben.dataset)
        ben.training(**cfg.preprocess)

        if cfg.preprocess['scale'] or cfg.preprocess['reduction'] or \
            cfg.preprocess['balance']:
            ben.dataset.plot(os.path.join(cfg.dirs['sortie'],
                                          'sig2_%s.png' % basename))
            print(ben.dataset)
        ben.run(optimisation=cfg.process['optimisation'],
                prediction=cfg.process['prediction'])

    report_writer.close()

if __name__ == "__main__":
    main(sys.argv[1:])

