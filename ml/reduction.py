# -*- coding: utf-8 -*-
"""
Created on Tue Oct 29 13:48:34 2013

@author: thomas_a
"""
from __future__ import print_function
import logging

import numpy as np
from sklearn import feature_selection
from sklearn import decomposition
from sklearn.discriminant_analysis import LinearDiscriminantAnalysis
from sklearn.svm import SVR

from maplearn.app.reporting import str_table
from maplearn.ml.machine import Machine


logger = logging.getLogger('maplearn.'+__name__)
estimator = SVR("linear")

class Reduction(Machine):
    """
    Reduit la dimensionnalité d'un jeu de données suivant plusieurs approches
    (ACP, LDA...)
    """

    def __init__(self, data, algorithms=None, **kwargs):

        Machine.ALL_ALGOS = dict( \
            PCA=decomposition.PCA(),
            KBEST=feature_selection.SelectKBest(feature_selection.f_classif),
            LDA=LinearDiscriminantAnalysis(),
            RFE=feature_selection.RFE(estimator=estimator, step=1),
            KERNEL_PCA=decomposition.KernelPCA(kernel="rbf",
                                               fit_inverse_transform=True))

        self.ncomp = 1  # nombre de dimensions souhaitees
        if 'ncomp' in kwargs:
            self.__nb_comp(kwargs['ncomp'])
        if 'features' in kwargs:
            self.__features = kwargs['features']
        else:
            self.__features = None

        Machine.__init__(self, algorithms=algorithms, data=data, **kwargs)
        self.algorithms = algorithms
        self.result = dict()

    def load(self, data):
        """
        Chargement des données
        """
        Machine.load(self, data)
        if self._data.Y is None:
            logger.warning('No samples specified')
        if self._data.data is None:
            self._data.data = self._data.X
        self.__check()

        if self.__features is None:
            if self._data.X is not None:
                last_col = self._data.X.shape[1] + 1
            else:
                last_col = self._data.data.shape[1] + 1
            features = range(1, last_col)
            self.__features = [str(i) for i in features]

    def __check(self):
        """
        Vérifie que les données sont compatibles avec la réduction de dimesions
        """
        if not isinstance(self._data.data, np.ndarray):
            raise TypeError("Les données doivent être sous la forme d'une \
                            matrice")
        elif self._data.data.ndim != 2:
            raise IndexError("Dataset should be 2D (%i dimensions found"
                             % self._data.data.ndim)
        elif self._data.data.shape[1] < self.ncomp:
            str_msg = "Dataset does not contain enough features (%i <= %i)" \
                      % (self._data.data.shape[1], self.ncomp)
            logger.critical(str_msg)
            raise IndexError(str_msg)
        else:
            logger.debug('Dataset ready to be reduced')

        if self._data.X is None or self._data.Y is None:
            logger.warning("X ou Y non renseigné => seront ignorés")
            self._data.X = None
            self._data.Y = None

        if self._data.X is not None:
            if self._data.X.ndim != self._data.data.ndim:
                raise IndexError("X n'a pas des dimensions compatibles avec \
                                 celles de data")
            elif self._data.X.shape[1] != self._data.data.shape[1]:
                raise IndexError("Le nombre de dimensions de X diffère de \
                                 celui de data")
        if self._data.Y is not None:
            if self._data.Y.ndim != 1:
                raise TypeError("Y doit être un vecteur d'échantillons")
            elif self._data.Y.shape[0] != self._data.X.shape[0]:
                raise IndexError("Le nombre d'individus dans Y est différent \
                                 de celui de X")

    def __nb_comp(self, n_feat):
        """
        Renvoie le nombre de dimensions à obtenir, si il n'est pas précisé par
        l'utilsiateur
        """
        if n_feat is not None:
            self.ncomp = n_feat
        elif self._data.Y is not None:
            ncomp = len(np.unique(self._data.Y))-1  # nb. de dimensions a conserver
            if ncomp < self.ncomp:
                ncomp = self.ncomp  # au moins nMin dimensions
        else:
            logger.warning('No information about number of components')

        # application de l'objectif [nombre de dimensions]
        if self.ncomp is not None:
            logger.info('Purpose : reducing to %i dimension(s)', self.ncomp)
            for i in ('k', 'n_features_to_select', 'n_components'):
                Machine._params[i] = self.ncomp
        return self.ncomp

    def fit_1(self, algo):
        """
        Mesure la qualité d'1 classification (par cross-validation)
        La cross-validation est ensuite appliquée
        """
        Machine.fit_1(self, algo)
        self.result['data'] = None
        self.result['X'] = None

        # liste des algos qui nécessitent des échantillons
        lst_algos = ('KBEST', 'LDA', 'RFE', 'KERNEL_PCA')
        if self._data.X is None or self._data.Y is None:
            if algo in lst_algos:
                str_msg = "%s Reduction needs samples" % algo
                logger.error(str_msg)
                return None

        # Entrainement
        if algo in lst_algos:
            try:
                self.clf.fit(self._data.X, self._data.Y)
            except ValueError as error:
                logger.critical(error.message)
                raise ValueError(error.message)
        elif algo == 'PCA':
            self.clf.fit(self._data.data)
        self._fitted = True

    def predict_1(self, algo):
        """
        Applique la réduction de dimensions par transformation ou selection
        des features
        """
        Machine.predict_1(self, algo)
        print('\n#### Reduction par %s ####\nObjectif : %i dimensions'
              % (self.clf, self.ncomp))

        if algo in ('LDA', 'PCA', 'KERNEL_PCA'):
            if self._data.X is not None:
                self.result['X'] = self.clf.transform(self._data.X)
            self.result['data'] = self.clf.transform(self._data.data)
        elif algo in ('RFE', 'KBEST'):
            # selection des features a conserver
            self.__features = [self.__features[c] for c in \
                               self.clf.get_support(True)]
            self.result['data'] = self._data.data[:, self.clf.get_support(True)]
            self.result['X'] = self._data.X[:, self.clf.get_support(True)]
        str_msg = 'Dimensions reduced from %i => %i' \
                  % (self._data.data.shape[1], self.result['data'].shape[1])
        logger.info(str_msg)
        self.__print(algo)
        print("\n"+str_msg)

    def run(self, predict=True, ncomp=None):
        """
        Application de la réduction de dimensions suivant la méthode spécifiée
        """
        if ncomp is not None:
            self.__nb_comp(ncomp)
        Machine.run(self, predict)
        return(self.result['data'], self.result['X'], self.__features)

    def __print(self, algo):
        """
        affichage textuel du resultat de la reduction
        """
        if algo == 'RFE':
            ranks = list(self.clf.ranking_)
            columns = [x for (_, x) in sorted(zip(ranks, self.__features))]

            print(str_table(header=['Features', 'Ranks'],
                            Ranks=[str(r) for r in ranks.sort()],
                            Features=columns))
        elif algo == 'KBEST':
            # tri par scores descendant
            scores = [self.clf.scores_[r] for r in self.clf.get_support(True)]
            pvalues = [self.clf.pvalues_[p] for p in \
                       self.clf.get_support(True)]
            columns = [x for (_, x) in sorted(zip(scores, self.__features),
                                              reverse=True)]
            pvalues = [x for (_, x) in sorted(zip(scores, pvalues),
                                              reverse=True)]
            scores.sort(reverse=True)
            print(str_table(header=['Features', 'Score', 'Pvalue'],
                            Features=self.__features,
                            Score=["%.2f" %s for s in scores],
                            Pvalue=["%.2g" %s for s in pvalues]))

        elif algo == 'PCA':
            print(str_table(header=['Composante', 'Variance'],
                            Composante=self.__features,
                            Variance=["%.1f%%" %(p*100) for p in \
                                      self.clf.explained_variance_ratio_]))
