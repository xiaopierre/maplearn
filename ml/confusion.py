# -*- coding: utf-8 -*-
"""
Created on Thu Jun 21 16:26:42 2012
Scripts pour sortir des matrices de confusion

@author: alban
"""
from __future__ import division, print_function

import os
import logging

import numpy as np
from sklearn.metrics import confusion_matrix, precision_score
import matplotlib.pyplot as plt
from pylab import rcParams
rcParams['figure.figsize'] = 8, 8



from app.reporting import str_extend

logger = logging.getLogger('maplearn.'+__name__)


class Confusion(object):
    """
    Calcul confusion entre 2 vecteurs :
    - y_sample
    - y_predit
    """
    def __init__(self, y_sample, y_predit, fTxt=None, fPlot=None):
        self.y_sample = y_sample
        self.y_predit = y_predit
        self.__files = {'txt':fTxt, 'plot':fPlot}
        self.__labels = np.union1d(y_sample, y_predit)
        # Resultats
        self.cm = None
        self.kappa = None
        self.score = None

    def calcul_matrice(self):
        """ calcule une matrice de confusion et affiche le resultat :
            - dans le terminal
        """
        # Compute confusion matrix
        self.cm = confusion_matrix(self.y_sample, self.y_predit,
                                   labels=self.__labels)
        try:
            if self.__labels.shape[0] == 2:
                self.score = precision_score(self.y_sample, self.y_predit,
                                             labels=self.__labels,
                                             pos_label=self.__labels[0],
                                             average='weighted')
            else:
                self.score = precision_score(self.y_sample, self.y_predit,
                                             labels=self.__labels,
                                             average='weighted')
        except:
            self.score = precision_score(self.y_sample, self.y_predit,
                                         average='weighted')
        self.calcul_kappa() # calcul du kappa
        return(self.cm, self.kappa) # renvoie la matrice de confusion

    def calcul_kappa(self):
        """ calcule l'indice de kappa a partir d'une table de contingence """

        total = float(np.sum(self.cm)) # somme totale

        f_po = np.sum(np.diagonal(self.cm)) / total # Probabilite observee

        # Probabilite estimee (liee uniquement au hasard)
        f_pe = 0
        for i in range(self.cm.shape[0]):
            f_pe += np.sum(self.cm[:, i]) * np.sum(self.cm[i, :])
        f_pe /= total ** 2
        self.kappa = (f_po - f_pe) / (1 - f_pe)
        return self.kappa

    def export(self, fTxt=None, fPlot=None, title=None):
        """
        Exporte la matrice de confusion
            - dans un fichier texte
            - dans un graphique
        """
        if not fTxt is None:
            self.__files['txt'] = fTxt
        if not fPlot is None:
            self.__files['plot'] = fPlot

        # enregistrement graphique de la matrice de confusion
        if not self.__files['plot'] is None:
            plt.figure(figsize=(30, 30))
            colmap = plt.cm.autumn_r
            colmap = plt.cm.copper_r
            colmap.set_under('white')
            img = plt.matshow(self.cm, cmap=colmap, vmin=1)

            if not title is None:
                plt.title(title)
            img.figure.text(.5, .12,
                            'Confusion Matrix on %i samples' \
                            % self.y_sample.shape[0],
                            fontsize=10, ha='center')
            img.figure.text(.5, .08,
                            'Precision score: %.3f | Kappa: %.3f | n=%i'
                            % (self.score, self.kappa, self.y_sample.shape[0]),
                            fontsize=10, ha='center')
            plt.colorbar()
            # labels des axes
            labels = [str(int(i)) for i in list(self.__labels)]
            plt.xticks(np.arange(len(labels)), labels, rotation=90, size=8)
            plt.yticks(np.arange(len(labels)), labels, size=8)
            #plt.show()     # affiche la figure

            # sauvegarde du graphique
            plt.savefig(self.__files['plot'], format='png',
                        bbox_inches='tight')
            plt.close() # ferme la figure
            img = None
            
            #print('![matrice confusion](%s)' % self.__files['plot'])
        else:
            logger.error('Export impossible : fichier image non défini')
        
        # ecriture dans un fichier texte
        if not self.__files['txt'] is None:
            # sauvegarde matrice de confusion (obligatoirement en premier)
            np.savetxt(self.__files['txt'], self.cm.astype(int), comments='',
                       header=np.array2string(self.__labels), fmt='%i')
            with open(self.__files['txt'], 'a') as ofi:
                ofi.write('Confusion Matrix on %i samples\n'
                          % self.y_sample.shape[0])
                ofi.write('Precision score: %.3f | Kappa: %.3f'
                          % (self.score, self.kappa))
        else:
            logger.error('Export impossible : fichier texte non défini')
        print(self)

    def __str__(self):
        labels = [str(int(i)) for i in list(self.__labels)]
        size = 3
        str_msg = '<div style="float:right;margin:0 10px 10px 0" markdown="1">'
        str_msg += '<img src=%s>' % os.path.basename(self.__files['plot'])
        str_msg += '</div>\n'
        str_msg += '|'+'|'.join([str_extend(txt, size) for txt in labels])
        str_msg += '|\n'
        str_msg += '|'+'|'.join(['-'*size,]*len(labels))
        str_msg += '|\n'
        # afficher la matrice de confusion sous forme texte
        str_format = '{: %id}' % size
        str_cm = np.array2string(self.cm, separator='|',
                                 formatter={'int':str_format.format})
        str_cm = str_cm.replace('[[', '|')
        str_cm = str_cm.replace(']]', '|\n')
        str_cm = str_cm.replace(']', '')
        str_cm = str_cm.replace(' [', '|')
        str_cm = str_cm.translate(None, '[]')
        str_msg += str_cm
        str_msg += '\nPrecision score: %.3f | Kappa : %.3f\n' % (self.score,
                                                                 self.kappa)
        #str_msg += '<img style="float: right;" src="%s">' % self.__files['plot']
        return str_msg


def confusion_cl(cm, labels, os1, os2):
    """
    calcule le pourcentage de confusion entre 2 classes donnees
    a partir d'une matrice de confusion donnee
    """
    # indices correspondantes aux 2 classes
    if os1 in labels and os2 in labels:
        id1 = long(np.where(labels == os1)[0])
        id2 = long(np.where(labels == os2)[0])
    else:
        return None
    # matrice de confusion sur les 2 classes
    cm_cl = np.matrix([[cm[id1, id1], cm[id1, id2]],
                       [cm[id2, id1], cm[id2, id2]]])

    #conf_cl=(cm_cl[0,1]+cm_cl[1,0])/np.sum(cm_cl)*100
    return (cm_cl[0, 1] + cm_cl[1, 0]) / np.sum(cm_cl)*100
