# -*- coding: utf-8 -*-
"""
Ecriture des données dans un fichier
Created on Wed Jun  4 12:53:48 2014

@author: thomas_a
"""
from __future__ import print_function

import os
import logging

from maplearn.filehandler.shapefile import Shapefile
from maplearn.filehandler.excel import Excel
from maplearn.filehandler.imagegeo import ImageGeo

logger = logging.getLogger('maplearn.'+__name__)

class Writer(object):
    def __init__(self, source, **kwargs):
        self.src = str(source).strip()
        self.__driver = None
        self.__to_file()
        self.__origin = None
        if 'origin' in kwargs:
            self.__origin = kwargs['origin']

    def __to_file(self):
        """
        Chargement de données à partir d'un fichier
        """
        logger.info('Lecture a partir du fichier : %s', self.src)
        s_ext = os.path.splitext(self.src)[1].lower()
        if s_ext in ['.xls', '.xlsx']:
            self.__driver = Excel(self.src)
        elif s_ext == '.shp':
            self.__driver = Shapefile(self.src)
        elif s_ext in ['.tif', '']:
            self.__driver = ImageGeo(self.src)
        else:
            raise IOError("Type de fichier inconnu : %s" % self.src)
        if self.__driver is not None:
            self.__driver.open_()

    def run(self, data, fichier):
        logger.info('Ecriture dans fichier %s...', fichier)
        self.__driver.write(fichier, data, origin=self.__origin)