# -*- coding: utf-8 -*-
"""
Conteneur des données avec :
- 1 colonne échantilon
- 1 ou plusieurs colonnes de données
Created on Sat Apr 19 10:15:16 2014

@author: thomas_a
"""
from __future__ import print_function

import os
from random import sample
import logging

import numpy as np
import pandas as pd
from sklearn.metrics import silhouette_samples
from sklearn import preprocessing

from maplearn.datahandler.echantillons import Echantillon
from maplearn.datahandler.signature import Signature
from maplearn.ml.reduction import Reduction
from maplearn.ml.distance import Distance
from maplearn.app.reporting import str_extend


logger = logging.getLogger('maplearn.'+__name__)

LIM_NFEAT = 15 # number of features that can be displayed at once


class PackData(object):
    """
    Conteneur des données avec :
    - Y = 1 colonne échantillon
    - X = données des échantillons avec x colonnes
    - data = données (hors échantillons) avec x colonnes
    """
    def __init__(self, X=None, Y=None, data=None, **kwargs):
        # échantillons :
        # Y=vecteur d'échantillons, X=matrice de données correspondantes
        self.__data = {'X':None, 'Y':None, 'data':None}
        self.not_nas = None # vecteur listant les donnees non NAs
        self.__metadata = {'codes':None, 'features':None,
                           'source':None, 'na':np.nan,
                           'outfile':'', 'images':''}
        self.__e = None
        for var in self.__metadata.keys():
            if var in kwargs:
                self.__metadata[var] = kwargs[var]

        self.load(X=X, Y=Y, data=data)

    @property
    def X(self):
        """
        Renvoie la matrice de données des échantillons
        """

        return self.__data['X']

    @X.setter
    def X(self, x):
        """
        Affectation des données associées aux échantillons
        """
        if x is None:
            self.__data['X'] = None
            logger.debug('X (re)initialized')
            return self.__data['X']
        if x.ndim != 2:
            logger.critical('Training should be a 2d matrix')

        # X is stored
        self.__data['X'] = np.copy(x)

        if self.__metadata['features'] is not None:
            if self.__data['X'].shape[1] != len(self.__metadata['features']):
                str_msg = 'Features (%i) in conflict with X (%i)' \
                         % (len(self.__metadata['features']),
                            self.__data['X'].shape[1])
                logger.critical(str_msg)
                raise IndexError(str_msg)
            else:
                self.features = self.__data['X']

    @property
    def Y(self):
        """
        Renvoie le vecteur des classes des échantillons
        """
        return self.__data['Y']

    @Y.setter
    def Y(self, y):
        """
        Affectation des échantillons
        """
        if y is None:
            self.__data['y'] = None
            logger.debug('Y (re)initialized')
            return self.__data['Y']

        if isinstance(y, list):
            y = np.array(y)

        # vérifie qu'il y a suffisamment de classes d'échantillons
        if len(np.unique(y)) <= 1:
            raise ValueError('Number of classes too low : %i',
                             len(np.unique(y)))
        if y.ndim != 1:
            str_msg = 'Samples should be a vector. Got %id data.' % y.ndim
            raise ValueError(str_msg)
        self.__e = Echantillon(y, self.__metadata['codes'])

        # Y is stored
        self.__data['Y'] = np.copy(y)

    @property
    def data(self):
        """
        Renvoie la matrice de données
        """
        return self.__data['data']

    @data.setter
    def data(self, data):
        """
        Définition du jeu de données
        """
        if data is None:
            logger.debug('Data (re)initialized')
            self.__data['data'] = None
            return self.__data['data']

        if data.ndim not in [2, 3]:
            raise IndexError('Data should be a 2d/3d matrix. %i dimension is\
                             not accepted' %data.ndim)
        self.__data['data'] = np.copy(data)

        # gestion des na
        self.not_nas = np.all(self.__data['data'] != self.__metadata['na'],
                              axis=1)
        n_na = len(self.not_nas[self.not_nas == False])
        if n_na >= self.__data['data'].shape[0]:
            s_msg = 'All data are considered NA [%s]' % self.__metadata['na']
            logger.critical(s_msg)
            raise ValueError(s_msg)
        elif n_na > 0:
            logger.warning('%i/%i (%.1f%%) NA values detected', n_na,
                           self.__data['data'].shape[0],
                           (n_na * 100.) / self.__data['data'].shape[0])

        # data is stored
        self.__data['data'] = np.copy(self.__data['data'][self.not_nas, :])

        # features
        if self.__metadata['features'] is not None:
            if self.__data['data'].shape[1] != len(self.__metadata['features']):
                str_msg = 'Features (%i) in conflict with dataset (%i)' \
                           % (len(self.__metadata['features']),
                              self.__data['data'].shape[1])
                logger.critical(str_msg)
                raise IndexError(str_msg)
        else:
            self.features = self.__data['data']

    @property
    def classes(self):
        """
        Renvoie la matrice de données
        """
        if self.Y is None:
            return None
        if self.__e.summary is None:
            self.__e.count()
        return self.__e.summary

    @property
    def features(self):
        """
        Renvoie la liste des features du jeu de données
        """
        return self.__metadata['features']

    @features.setter
    def features(self, items):
        """
        Définition de la liste des features à partir de données
        """
        if isinstance(items, list):
            self.__metadata['features'] = [str(i) for i in items]
        elif isinstance(items, np.ndarray):
            self.__metadata['features'] = [str(i) for i in \
                                           range(items.shape[1])]
        elif isinstance(items, pd.DataFrame):
            self.__metadata['features'] = [str(i) for i in items.columns]
        else:
            raise TypeError('Data should be an array or a dataframe')
        logger.info('Dataset with %i features',
                    len(self.__metadata['features']))

    def load(self, X=None, Y=None, data=None, features=None):
        """
        Affecte les données
        """
        logger.debug('Chargement de données...')
        if X is not None and Y is not None:
            self.X = X
            self.Y = Y

            # vérifie que les dimensions de X et Y sont compatibles
            if X.shape[0] != Y.shape[0]:
                str_msg = "Number of samples (%i) in conflict with \
                          number of datas (%i)" % (Y.shape[0], X.shape[0])
                logger.critical(str_msg)
                raise IndexError(str_msg)

        if data is not None:
            self.data = data

        if features is not None:
            self.features = features

        logger.info('Datas loaded')
        self.__check()

    def __check(self):
        """
        Verifie compatibilite entre X, Y et data
        """
        logger.debug('Checking dataset...')
        if self.X is None and self.Y is None and self.data is None:
            raise ValueError("Dataset is empty")

        # vérifie que les dimensions de X et Y sont compatibles
        if self.X is not None or self.Y is not None:
            if self.X.shape[0] != self.Y.shape[0]:
                raise IndexError("Numbers of features in data (%i) \
                                 and samples (%i) are different",
                                 self.X.shape[0], self.Y.shape[0])

        if self.data is not None and self.X is not None:
            if self.data.shape[1] != self.X.shape[1]:
                raise IndexError('Numbers of features in data (%i) and \
                                 samples (%i) are different',
                                 self.data.shape[1], self.X.shape[1])

        logger.info('Dataset checked => OK')

    def scale(self):
        """
        Normalisation des donnees
        """
        logger.debug('Normalizing dataset...')
        for i in ('data', 'X'):
            if self.__data[i] is not None:
                self.__data[i] = preprocessing.scale(self.__data[i])
        logger.debug('Dataset normalized')

    def reduit(self, meth='LDA', ncomp=None):
        """
        Reduit les dimensions du jeu de données
        """
        #TODO : placer la condition self.__data['data'] is None dans Reduction
        #   plutôt que dans Packdata ?
        logger.debug('Reduction des dimensions du jeu de données')

        red = Reduction(data=self.__data, algorithms=meth,
                        features=self.features)
        if self.data is not None:
            (self.data, self.X, self.features) = red.run(ncomp)
        else:
            (self.X, _, self.features) = red.run(ncomp)

    def separabilite(self, metric='euclidean'):
        """
        Vérifie la séparabilité entre les échantillons
        """
        logger.debug('Analyse de separabilité (%s)...', metric)
        dist = Distance()
        mat_dist = dist.run(x=self.__data['X'], meth=metric)
        sep = silhouette_samples(X=mat_dist, labels=self.__data['Y'],
                                 metric="precomputed")
        print('\n## ANALYSE DE SEPARABILITE (%s)\n' %metric)
        size = 16
        print(' | '.join([str_extend(txt, size) for txt in \
                         ('Classe', 'mean+/-sd', 'min', 'max')]))
        print('-|-'.join(['-'*size,]*4))
        for i in np.unique(self.__data['Y']):
            sep_cl = sep[np.where(self.__data['Y'] == i)]
            i = str(int(i))
            # Recode les valeurs pour affichage
            if np.mean(sep_cl) > .5:
                str_msg = "(++)"
            elif np.mean(sep_cl) > .25 and np.mean(sep_cl) <= .5:
                str_msg = "(+)"
            elif np.mean(sep_cl) < -.25 and np.mean(sep_cl) >= -.5:
                str_msg = "(-)"
            elif np.mean(sep_cl) < -.5:
                str_msg = "(--)"
            else:
                str_msg = ""
            print(' | '.join([str_extend(txt, size) for txt in
                              ('%s' % i, '%.2f+/-%.2f %s'
                               % (np.mean(sep_cl), np.std(sep_cl), str_msg),
                               '%.2f' % np.min(sep_cl),
                               '%.2f' % np.max(sep_cl))]))
        print('[-1:confus | 0: chevauchement | +1: séparés]\n')

    def balance(self, seuil=None):
        """
        Equilibre les echantillons pour limiter les classes trop preponderantes
        """
        # convertir en pandas df => + facile pour faire la suite
        logger.debug('Rééquilibrage des échantillons...')
        a_count = np.array(self.__e.summary.values())
        if seuil is not None:
            seuil = int(seuil)
        else:
            # TODO : trouver ref avec regle empirique pour definir ce seuil
            if len(self.__e.summary) > 3:
                # seuil défini par défaut = triple de la fréquence médiane
                seuil = int(np.median(a_count) * 3)
            else:
                # seuil quand peu de classes = nbres d'échantillons dans la
                # classe avec le moins d'échantillons
                seuil = int(np.min(a_count))
        if np.max(a_count) <= seuil:
            print("Reequilibrage des echantillons (seuil : %i) => inutile" \
                  % seuil)
        else:
            print("Reequilibrage des echantillons (seuil : %i)" % seuil)
            a_idx = np.ones(self.__data['Y'].shape, dtype=np.byte)
            # Liste des classes dominantes (> seuil)
            lst_cl_dom = [k for k, v in self.__e.summary.items() if v > seuil]
            for l_os in lst_cl_dom:
                a_idx_os = np.where(self.__data['Y'] == l_os)
                print("Classe %i : %i => %i echantillons a eliminer"
                      % (l_os, len(a_idx_os[0]), len(a_idx_os[0]) - seuil))
                # selection aleatoire des echantillons a eliminer (code 0)
                a_idx_sel = sample(range(len(a_idx_os[0])),
                                   len(a_idx_os[0]) - seuil)
                a_idx[a_idx_os[0][a_idx_sel]] = 0
            # application de la selection
            self.__data['Y'] = np.copy(self.__data['Y']
                                       [np.where(a_idx == 1)[0]])
            self.__data['X'] = np.copy(self.__data['X']
                                       [np.where(a_idx == 1)[0], :])
            # description des echantillons apres equilibrage
            self.__e.Y = self.__data['Y']
        logger.info('Echantillons rééequilibrées')

    def plot(self, out_file):
        """
        Représentation graphique de la signature spectrale :
        * du jeu de données
        * de chaque classe d'échantillons par rapport au jeu de données
        """
        self.__metadata['outfile'] = out_file
        logger.debug('Création des graphique du jeu de données')
        sig = Signature()
        str_title = 'Signature spectrale du jeu de donnees\n'
        if self.data is None and self.X is not None:
            data = self.X
        elif self.X is not None:
            data = self.data
        else:
            logger.error('Aucune donnée disponible')
            return

        if data.shape[1] > LIM_NFEAT:
            sig.plot(data=data, title=str_title,
                     out_file=self.__metadata['outfile'], features=None)
        else:
            sig.boxplot(data=data, title=str_title,
                        out_file=self.__metadata['outfile'],
                        features=self.__metadata['features'])

        if self.X is not None and self.Y is not None:
            self.__metadata['images'] = ''
            for i in np.unique(self.Y):
                str_title = 'Signature spectrale : classe %i\n' % i
                str_file = self.__metadata['outfile'].replace('.', 'cl%i.' % i)
                if data.shape[1] <= LIM_NFEAT:
                    sig.boxplot_classe(data=self.X,
                                       data_classe=self.X[self.Y == i, :],
                                       title=str_title,
                                       out_file=self.__metadata['outfile'],
                                       features=self.__metadata['features'])
                    self.__metadata['images'] += '![%s](%s)' \
                        % (str_title, os.path.basename(str_file))
        logger.info('Graphique(s) du jeu de données créé(s)')

    def __str__(self):

        str_msg = "\n### Description du jeu de donnees ###\n"
        if self.__data['data'] is None and self.__data['X'] is None:
            str_msg += '\t=> Aucune donnée disponible'
        else:
            if self.__data['data'] is None:
                n_data = self.__data['X'].shape[0]
                n_feat = self.__data['X'].shape[1]
                #stats = self.__data['X'].describe()
            else:
                n_data = self.__data['data'].shape[0]
                n_feat = self.__data['data'].shape[1]
                #stats = self.__data['data'].describe()
            str_msg += '<div style="float:right;margin:0 10px 10px 0" \
                       markdown="1">'
            str_msg += '<img src=%s>' \
                       % os.path.basename(self.__metadata['outfile'])
            str_msg += '</div>'
            str_msg += "* %i donnees\n" % n_data
            str_msg += "* %i features" % n_feat
            if self.features is not None:
                str_msg += ' : '
                if len(self.features) > 5:
                    str_msg += '; '.join(self.features[:5])
                    str_msg += '...\n'
                else:
                    str_msg += '; '.join(self.features) + '\n'
        str_msg += str(self.__e) + '\n'
        str_msg += self.__metadata['images'] + '\n'
        #str_msg += stats + '\n'
        return str_msg
