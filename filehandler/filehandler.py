# -*- coding: utf-8 -*-
"""
Created on Thu Mar 10 09:24:05 2016

@author: thomas_a
"""
import logging
import os

logger = logging.getLogger('maplearn.'+__name__)


class FileHandler(object):
    """
    Communication avec fichier physique :
    - ecriture de donnees dans un fichier
    - lecture de donnees a partir de fichier
    """
    def __init__(self, path=None, **kwargs):
        self.__dsn = {'path':None} # source de donnees (dictionnaire)
        self._drv = None # driver (si necessaire)
        self._data = None # jeu de donnees (a recuperer ou ecrire)
        self.opened = False # Indique si le jeu de donnees est ferme ou ouvert
        kwargs['path'] = path
        self.__set_dsn(**kwargs)

    @property
    def dsn(self):
        """
        Renvoie la source de données liées
        """
        return self.__dsn

    @dsn.setter
    def dsn(self, **kwargs):
        self.__set_dsn(kwargs)

    def __set_dsn(self, **kwargs):
        """
        Definit le fichier à lire ou écrire
        """
        # reinitialisation source de donnees
        if 'path' in kwargs and not kwargs['path'] is None:        
            logger.info('Fichier : %s', self.__dsn['path'])

        for key, value in kwargs.iteritems():
            self.__dsn[key] = value
        if self.__dsn['path'] is None:
            logger.warning("Aucun fichier défini")
    @property
    def data(self):
        """
        Renvoie la source de données liées
        """
        return self._data

    @data.setter
    def data(self, data):
        """
        Affecte le jeu de donnees
        """
        self._data = data

    def open_(self):
        """
        Ouverture d'un fichier
        """
        self.opened = False
        if not os.path.exists(self.__dsn['path']):
            raise IOError('Fichier %s absent => ne peut etre lu' % \
                          self.__dsn['path'])
        self.opened = True

    def read(self):
        """
        Lecture des données à partir du fichier
        """
        self._data = None
        if not self.opened:
            self.open_()
        logger.debug('Lecture du fichier %s...', self.__dsn['path'])

    def write(self, path=None, data=None, overwrite=True):
        """
        Ecriture des donnees dans un fichier
        """
        if not path is None:
            self.__set_dsn(path=path)
        if not data is None:
            self.data = data
        
        if self.data is None:
            raise ValueError('Aucune donnee a ecrire')

        if os.path.exists(self.__dsn['path']):
            if overwrite:
                logger.warning('Le fichier %s existe deja et va etre ecrase',
                                self.__dsn['path'])
            else:
                str_msg = 'Le fichier %s existe deja' % self.__dsn['path']
                logger.error(str_msg)
                raise IOError(str_msg)

    def __str__(self):
        str_msg = ""
        lst_param = ['path', 'format']
        lst_param += [k for k in self.__dsn.keys() if k not in lst_param]
        for p in lst_param:
            str_msg += "%s : " % p.capitalize()
            if p in self.__dsn and not self.__dsn[p] is None:
                str_msg += "%s\n" % self.__dsn[p]
            else:
                str_msg += "Inconnu\n"

        if not self._data is None:
            str_msg += 'Donnees en stock : %i lignes - %i colonnes' % \
                       (self._data.shape[0], self._data.shape[1])
        return str_msg

    def __del__(self):
        self._drv = None