# -*- coding: utf-8 -*-
"""
Created on Tue May 20 10:13:31 2014

@author: thomas_a
"""
from __future__ import print_function

import logging
import pandas as pd

from maplearn.filehandler.filehandler import FileHandler

logger = logging.getLogger('maplearn.'+__name__)

pd.core.format.header_style = None  # <--- Workaround for header formatting


class Excel(FileHandler):
    """
    Driver pour manipuler des fichiers Excel
    """
    def __init__(self, path, sheet=None):
        super(Excel, self).__init__(path=path, sheet=sheet, format='Excel')

    def open_(self):
        """
        Ouverture d'un fichier Exccel
        """
        FileHandler.open_(self)

        self._drv = pd.ExcelFile(self.dsn['path'])

        # comportement par defaut : si nom de la feuille pas trouvee, on prend
        # la premiere du classeur
        if self.dsn['sheet'] is None:
            logger.debug('Pas de feuillet defini => Utilisation du 1er')
            self.dsn['sheet'] = self._drv.sheet_names[0]
        if self.dsn['sheet'] not in self._drv.sheet_names:
            logger.debug('Feuillet %s absent => Utilisation du 1er',
                         self.dsn['sheet'])
            self.dsn['sheet'] = self._drv.sheet_names[0]

    def read(self):
        """
        Lecture des données à partir du fichier Excel
        """
        FileHandler.read(self)
        # lecture du feuillet
        self.data = self._drv.parse(self.dsn['sheet'])
        logger.info(self)
        return self.data

    def write(self, path=None, data=None, overwrite=True):
        """
        Lecture des données à partir du fichier Excel
        """
        FileHandler.write(self, path=path, data=data, overwrite=overwrite)
        # lecture du feuillet
        if data is None:
            data = self.data
        # Create a Pandas Excel writer using XlsxWriter as the engine.
        writer = pd.ExcelWriter(path)
        data.to_excel(writer)
        writer.save()
        logger.info('Fichier Excel écrit (%s)', path)
