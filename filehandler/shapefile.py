# -*- coding: utf-8 -*-
"""
Lecture d'un fichier shapefile contenant des échantillons et des données
Dans le vue d'une classification

Source : "Tutoriel Programmation SIG, utilisation des outils Python libres"
de T. Guyet

@author: alban
"""
#from __future__ import unicode_literals

import os
import logging

import pandas as pd
import numpy as np
from osgeo import ogr

ogr.GetUseExceptions()

from maplearn.filehandler.filehandler import FileHandler
# TODO : recherche l'encodage pour les caracteres...

logger = logging.getLogger('maplearn.'+__name__)

DCT_OGR_TYPES = {ogr.OFTString: [str, ],
                ogr.OFTInteger: [int, np.int, np.int8, np.int16, np.int32, np.int64],
                ogr.OFTReal: [float, np.float, np.float32, np.float64,
                          np.float128]}

class Shapefile(FileHandler):
    """
    Lecture et écriture de shapefile
    """
    def __init__(self, path):
        super(Shapefile, self).__init__(path=path, format='Shapefile')
         # driver pour pouvoir lire/écrire un fichier shapefile
        self._drv = ogr.GetDriverByName('ESRI Shapefile')
        self.__ds = None
        self.__lyr = None
        self.str_type = None  # type de géométrie
        self.lst_flds = None
        self._nb_feat = 0

    def open_(self):
        """
        Ouverture du shapefile
        """
        FileHandler.open_(self)
        self.__ds = self._drv.Open(self.dsn['path'])
        self.__lyr = self.__ds.GetLayer()
        self._nb_feat = self.__lyr.GetFeatureCount()
        self.str_type = ogr.GeometryTypeToName(self.__lyr.GetGeomType())

        self.lst_flds = self.__lyr.GetFeature(0).keys()
        logger.info('Shapefile chargé : %i %s(s) - %i Attributs',
                     self._nb_feat, self.str_type, len(self.lst_flds))

    def read(self):
        """ Lecture des entités du shapefile
        """
        FileHandler.read(self)
        self.data = pd.DataFrame(index=np.arange(self._nb_feat),
                                 columns=self.lst_flds)
        for i in range(self._nb_feat):
            feat = self.__lyr.GetFeature(i)
            for str_fld in self.lst_flds:
                self.data.set_value(i, str_fld, feat.GetField(str_fld))
        logger.info('Données lues (%i*%i)', self.data.shape[0],
                     self.data.shape[1])
        return self.data

    def __get_fld_type(self, column):
        """
        Renvoie type de champs (ogr) d'après type de champs (dataframe)
        """
        type_ogr = None
        type_feat = type(self.data[column][0])
        if self.data[column][0] is None:
            type_ogr = ogr.OFTString
        else:
            for k in DCT_OGR_TYPES.keys():
                if type_feat in DCT_OGR_TYPES[k]:
                    type_ogr = k
                    logger.debug('Found field type : %s', DCT_OGR_TYPES[k])
                    break
        if type_ogr is None:
            str_msg = 'Field %s : unknow type (%s)' % (column, type_feat)
            logger.critical(str_msg)
            raise TypeError(str_msg)
        return type_ogr

    def write(self, path=None, data=None, overwrite=True, **kwargs):
        """
        Ecriture des données attributaires en utilisant la
        géométrie d'un shapefile
        """
        """
        if 'origin' in kwargs and not kwargs['origin'] is None:
            self.dsn['path'] = kwargs['origin']
            data = self.read()
        else:
            raise IOError('Cannot write shapefile without geometries')
        """
        """
        if self.data is None:
            self.read()
        ori_data = self.data
        """
        FileHandler.write(self, path=path, data=data, overwrite=overwrite)
        
        # creation du fichier
        if overwrite and os.path.exists(path):
            self._drv.DeleteDataSource(path)
        ds_out = self._drv.CreateDataSource(path)

        if ds_out is None:
            raise IOError("Impossible de créer le fichier %s" % path)

        # creation de la structure de la couche
        str_lyr = os.path.splitext(os.path.basename(path))[0]
        lyr_out = ds_out.CreateLayer(str_lyr, geom_type=ogr.wkbPolygon,
                                     srs=self.__lyr.GetSpatialRef())
        # creation de la table
        for name in data.columns:
            type_ogr = self.__get_fld_type(name)
            fdefn = ogr.FieldDefn(name, type_ogr)
            lyr_out.CreateField(fdefn)
            logger.debug('Column added to table definition : %s (%s)', name, type_ogr)
        fld_defn = lyr_out.GetLayerDefn()
        logger.info('Table defined : %i columns', len(data.columns))

        # ecriture des données
        feat_in = self.__lyr.GetNextFeature()
        i = 0
        while i < self._nb_feat:
            feat_out = ogr.Feature(fld_defn)
            feat_out.SetGeometry(feat_in.GetGeometryRef())
            for name in data.columns:
                try:
                    feat_out.SetField(name, data.at[i, name])
                except NotImplementedError:
                    feat_out.SetField(name, np.asscalar(data.at[i, name]))
            lyr_out.CreateFeature(feat_out)

            feat_in.Destroy()
            feat_out.Destroy()

            # incrementation
            feat_in = self.__lyr.GetNextFeature()
            i += 1
        logger.info('Ecriture de %i entites dans %s', i + 1, path)
        ds_out = None

    def __del__(self):
        super(Shapefile, self).__del__()
        self.__ds = None
        self.__lyr = None
