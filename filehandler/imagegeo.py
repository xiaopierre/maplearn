# -*- coding: utf-8 -*-
"""
Image Géographique = association entre :
    - une matrice de pixels contenant des sonnées
    - une géographie (composée d'un système de projection et d'un étendue)
Created on Thu Oct 24 16:39:59 2013

@author: thomas_a
"""
import logging

from osgeo import gdal, gdalnumeric, osr, gdal_array
import numpy as np
import pandas as pd

gdal.UseExceptions()

from maplearn.filehandler.filehandler import FileHandler

logger = logging.getLogger('maplearn.'+__name__)

# Table de conversion vers Gdal types
NP2GDAL = {
  "uint8": 1,
  "int8": 1,
  "uint16": 2,
  "int16": 3,
  "uint32": 4,
  "int32": 5,
  "float32": 6,
  "float64": 7,
  "complex64": 10,
  "complex128": 11,
}


class ImageGeo(FileHandler):
    """
    Manipulation d'image geographique
    """
    def __init__(self, path=None, fmt='GTiff'):
        super(ImageGeo, self).__init__(path=path, format=fmt)
        self.__geo = {'transf':None, 'prj':None} # attributs geographiques (projection...)
        self.dims = None

    def read(self, dtype=None):
        """
        Chargement de la matrice de pixels à partir d'un fichier
        """
        FileHandler.read(self)
        try:
            data = gdalnumeric.LoadFile(self.dsn['path'])
        except RuntimeError:
            raise IOError('Fichier %s illisible' % self.dsn['path'])

        self.data(np.transpose(data), dtype)
        logger.info("Image %s chargee", self.dsn['path'])

    def read_geo(self, dtype=None):
        """
        Chargement de l'image géographique (matrice de pixels + géographie) à
        partir d'un fichier
        """
        FileHandler.read(self)
        try:
            dataset = gdal.Open(self.dsn['path'])
        except RuntimeError:
            raise IOError('Fichier %s illisible' % self.dsn['path'])

        self.__geo['transf'] = dataset.GetGeoTransform()
        self.__geo['prj'] = dataset.GetProjectionRef()
        # chargement de la première bande
        band = dataset.GetRasterBand(1).ReadAsArray()
        if dtype is None:
            dtype = band.dtype
            logger.debug('Dtype is %s', dtype)
        
        if dataset.RasterCount == 1:
            data = np.transpose(band)
        else:
            data = np.empty((dataset.RasterXSize, dataset.RasterYSize,
                             dataset.RasterCount), dtype=dtype)
            i = 1
            while i <= dataset.RasterCount:
                data[:, :, i-1] = np.transpose( \
                    dataset.GetRasterBand(i).ReadAsArray())
                i += 1

        dataset = None
        logger.info("Image %s chargee", self.dsn['path'])
        self.data(data, dtype)

    def set_geo(self, transf=None, prj=None):
        """
        Définit la géographie d'une image : son étendue et son système de
        projection

        A FAIRE : ajouter une vérification de l'adéquation entre étendue et
        dimension de l'image

        Définition de transf (pour caler une image) :
        [0] = top left x (x Origin)
        [1] = w-e pixel resolution (pixel Width)
        [2] = rotation, 0 if image is "north up"
        [3] = top left y (y Origin)
        [4] = rotation, 0 if image is "north up"
        [5] = n-s pixel resolution (pixel Height)
        """
        if not transf is None:
            self.__geo['transf'] = transf
        if not prj is None:
            self.__geo['prj'] = prj
        # TODO : ajouter le code EPSG ici et alléger(un peu) write()

    def init_data(self, dims, dtype=None):
        """
        Initialise la matrice de l'image
        """
        if not dtype is None:
            self.data(np.zeros(dims, dtype), dtype)
        else:
            self.data(np.zeros(dims))
    
    def data(self, data, dtype=None):
        """
        Ecrase le jeu de données avec une matrice en entrée
        """
        #gdal_array.NumericTypeCodeToGDALTypeCode(dtype)
        if dtype is None:
            self.dsn['depth'] = np.typeDict[str(data.dtype)]
        else:
            self.dsn['depth'] = dtype
            data = np.copy(data).astype(dtype)
        self.data = data
        self.dims = data.shape
        logger.info("Image a %i dimensions (%s)", data.ndim,
                    self.dsn['depth'])


    def xy2pixel(self, lon, lat):
        """
        Renvoie la position j,i dans l'image (en colonne,ligne) correspondant
        à une coordonnée géographique X,Y donnée


        Uses a gdal geomatrix (gdal.GetGeoTransform()) to calculate
        the pixel location of a geospatial coordinate
        (http://geospatialpython.com/2011/02/clip-raster-using-shapefile.html)
        """  
        lon, lat = float(lon), float(lat)
        ul_lon = self.__geo['transf'][0]
        ul_lat = self.__geo['transf'][3]
        lon_delta = self.__geo['transf'][1]
        lat_delta = abs(self.__geo['transf'][5])
        j = int(round((ul_lat - lat) / lat_delta)) # coordonnée en ligne
        i = int(round((lon - ul_lon) / lon_delta)) # coordonnée en colonne

        if i < 0 or i > self.dims[0] or j < 0 or j > self.dims[1]:
            logger.warning("(%f,%f) hors de l'image", lon, lat)
            return None
        return (i, j)

    def pixel2xy(self, j, i):
        """
        Renvoie la coordonnee geographique d'une coordonnee pixel
        (colonnes, lignes) en utilisant la geomatrix
        (self.__geo['transf']) de GDAL (gdal.GetGeoTransform())
        source initiale :
        http://geospatialpython.com/2011/02/clip-raster-using-shapefile.html
        """
        if i < 0 or i > self.dims[1] or j < 0 or j > self.dims[0]:
            logger.warning("(%i,%i) hors de l'image (%i,%i)", i, j,
                            self.dims[0], self.dims[1])
            return None
        i, j = int(i), int(j)
        ul_lon = self.__geo['transf'][0]
        ul_lat = self.__geo['transf'][3]
        lon_delta = self.__geo['transf'][1]
        lon_dist = self.__geo['transf'][5]
        lon = (ul_lon + (j * lon_delta))
        lat = (ul_lat + (i * lon_dist))
        return (lon, lat)

    def write(self, path=None, data=None, overwrite=True, **kwargs):
        """
        Export de l'image dans un raster
        """
        if data is None:
            data = self.data
        if 'origin' in kwargs and not kwargs['origin'] is None:
            self.dsn['path'] = kwargs['origin']
            self.read_geo()
            data = self.data_2_img(data, True)
        FileHandler.write(self, path, data, overwrite)
        ### Création du fichier raster en sortie
        if self._drv is None:
            self._drv = gdal.GetDriverByName(self.dsn['format'])
        
        if 'epsg' in kwargs:
            self.__geo['epsg'] = kwargs['epsg']
        if 'epsg' in self.__geo:
            logger.debug('ESPG used to write : %s', self.__geo['depth'])            
        """
        elif 'epsg' not in self.__geo:
            self.__geo['epsg'] = 2154 # valeur par defaut
        """
        dtype = None
        if 'depth' in kwargs:
            self.dsn['depth'] = kwargs['depth']
            logger.debug('Depth pixel used to write : %s', self.dsn['depth'])

        try:
            dtype = NP2GDAL[self.dsn['depth']] # conversion en type utilisable par GDAL            
        except KeyError:
            pass
        if dtype is None:
            try:
                dtype = gdal_array.NumericTypeCodeToGDALTypeCode(self.dsn['depth'])
            except TypeError:
                dtype = gdal.GDT_Float64
                logger.error('dtype expected, got %s => "Float64" \
                             will be used instead', self.dsn['depth'], dtype)
        
        if data.ndim == 2:
            dst_ds = self._drv.Create(self.dsn['path'], self.dims[0],
                                      self.dims[1], 1, dtype)
        elif data.ndim == 3:
            dst_ds = self._drv.Create(self.dsn['path'], self.dims[0],
                                      self.dims[1], data.shape[2], dtype)
        else:
            raise ValueError("Impossible d'ecrire 1 image à %i dimension(s)" \
                             % self.data.ndim)

        # calage
        if not self.__geo['transf'] is None:
            dst_ds.SetGeoTransform(self.__geo['transf'])
        
        # applique le systeme de projection
        if not self.__geo['prj'] is None:
            dst_ds.SetProjection(self.__geo['prj'])
        elif 'epsg' in self.__geo:
            sref = osr.SpatialReference()
            sref.ImportFromEPSG(self.__geo['epsg'])
            dst_ds.SetProjection(sref.ExportToPrettyWkt())
        else:
            logger.warning('No projection system given')
        # export
        if self.data.ndim == 2:
            dst_ds.GetRasterBand(1).WriteArray(np.transpose(self.data))
        elif self.data.ndim == 3:
            for i in range(self.dims[2]):
                try:
                    dst_ds.GetRasterBand(i+1).WriteArray( \
                        np.transpose(self.data[:, :, i]))
                except RuntimeError:
                    logger.warning('Issue to write band %i', i)
        dst_ds = None
        logger.info('Image %s enregistree', self.dsn['path'])
    
    def data_2_img(self, data, overwrite=False):
        """
        Remet en forme un jeu de données (dataframe pandas) en matrice
        pour pouvoir l'exporter sous forme d'image
        (opération inverse à la méthode __img_2_data())
        """
        logger.debug('Transformation de la matrice')
        if isinstance(data, pd.DataFrame):
            # conversion dataframe => matrice
            data = data.as_matrix()
        
        if data.ndim == 1 or data.shape[1] == 1:
            img = np.reshape(data, self.dims[:2])
        elif data.shape[1] > 1:
            img = np.reshape(data, (self.dims[0], self.dims[1], data.shape[1]))
        if overwrite:
            self.data = img
        return img

    def img_2_data(self):
        """
        Met les données en forme pour qu'elles soient plus facilement utilisables
        dans la suite des traitements
        Convertie les matrices issues des images en matrices à 2 dimensions
        (où 1 ligne = 1 individu et 1 colonne = 1 feature)
        """
        if self.data.ndim == 3:
            data = pd.DataFrame(np.reshape(self.data,
                            (self.dims[0] * self.dims[1], self.dims[2])))
        else:
            data = pd.DataFrame(self.data)
        #self.data = data
        return data