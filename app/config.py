#! /usr/bin/env python
# -*- coding: utf-8 -*-
"""
Configuration de Benchmark Classification
Created on Tue Jul  7 13:50:10 2015

@author: thomas_a
"""
import os
import ConfigParser
import logging

import logging.config
logging.config.fileConfig('logging.cfg', disable_existing_loggers=True)

logger = logging.getLogger('maplearn.'+__name__)

def splitter(text):
    """
    Découpe une chaine de caractères en fonction de plusieurs séparateurs
    possibles, et élimine les espaces superflus
    """
    if text is None:
        return text
    for sep in [',', ';', '|']:
        if sep in text:
            text = text.split(sep)
            break
    if type(text) == str:
        text = [text,]
    text = [i.strip() for i in text]
    return text

def cvert_bool(text):
    """
    Convertie une entrée en texte sous forme de booléen
    """
    if text is None:
        text = False
    if type(text) == bool:
        pass
    elif type(text) == str:
        if text.lower() in ['t', 'true', '1']:
            text = True
        else:
            text = False
    else:
        raise TypeError('Text expected. Got %s' % type(text))
    return text

def cvert_text(text):
    """
    Met en forme et nettoie un texte donné en entrée
    """
    try:
        text = text.strip()
    except AttributeError:
        pass
    if text == '':
        text = None
    return text


class Config(object):
    """
    Charge et verifie la configuration de l'application à partir d'un fichier
    de configuration (file_config)
    """
    __METADATA = dict(
            preprocess = {'scale':'Scale',
                          'balance':'Reequilibrage des echantillons',
                          'separabilite':'Analyse de separabilite des \
                          echantillons'})
    entree = dict()
    preprocess = {'scale':True, 'balance':False, 'reduction':None,
                  'ncomp':None, 'separabilite':False}
    process = {'type':None, 'kfold':None}
    dirs = dict()

    def __init__(self, file_config):
        if not os.path.exists(file_config):
            str_msg = "Fichier de configuration absent (%s)" % file_config
            logger.critical(str_msg)
            raise ConfigParser.Error(str_msg)
        self._file_cfg = file_config
        self.read()

    def check(self):
        """
        Verification que les paramètres sont OK
        """
        nb_pbs = 0
        if not os.path.exists(self.dirs['bin']):
            logger.critical("Le dossier des executables (%s) n'existe pas.",
                            self.dirs['bin'])
            nb_pbs += 1
        if not os.path.exists(self.dirs['sortie']):
            os.makedirs(self.dirs['sortie'])

        # Conversion en Booléen
        for param in ['scale', 'balance', 'separabilite']:
            self.preprocess[param] = cvert_bool(self.preprocess[param])
        for param in ['optimisation', 'prediction']:
            self.process[param] = cvert_bool(self.process[param])

        # liste des source(s) en entree
        for i in ['echantillons', 'features']:
            self.entree[i] = splitter(self.entree[i])
        if not self.entree['echantillons'] is None:
            for fichier in self.entree['echantillons']:
                if fichier in ['iris', 'digits']:
                    continue
                if not os.path.exists(fichier):
                    logger.critical("Fichier source (%s) absent", fichier)
                    nb_pbs += 1

        if not self.entree['data'] is None:
            if not os.path.exists(self.entree['data']):
                logger.critical("Fichier source (%s) absent",
                                self.entree['data'])
                nb_pbs += 1

        # liste des algorithme(s)
        self.process['algorithm'] = splitter(self.process['algorithm'])

        # Traduction des textes vides en None
        for param in ['classe', 'classe_id', 'na']:
            self.entree[param] = cvert_text(self.entree[param])
        for param in ['reduction', ]:
            self.preprocess[param] = cvert_text(self.preprocess[param])

        return nb_pbs

    def read(self):
        """
        Recupere la configuration de la station meteo telle qu'enregistree dans
        le fichier de configuration
        """
        cfgparser = ConfigParser.ConfigParser()
        cfgparser.read(self._file_cfg)

        # Définition des données en entrée
        lst = ['echantillons', 'classe_id', 'classe', 'features', 'data', 'na']
        for param in lst:
            try:
                self.entree[param] = cfgparser.get('entree', param).strip()
            except ConfigParser.NoOptionError:
                self.entree[param] = None
            else:
                if self.entree[param] == '':
                    self.entree[param] = None

        # Pretraitements
        lst = ['scale', 'reduction', 'separabilite', 'balance']
        for param in lst:
            try:
                self.preprocess[param] = cfgparser.get('pretraitement',
                                                       param).strip()
            except ConfigParser.NoOptionError:
                self.preprocess[param] = None

        try:
            self.preprocess['ncomp'] = cfgparser.getint('pretraitement',
                                                        'ncomp')
        except ValueError:
            pass

        lst = ['type', 'distance', 'algorithm', 'optimisation', 'prediction']
        for param in lst:
            self.process[param] = cfgparser.get('traitement', param).strip()
        try:
            self.process['kfold'] = cfgparser.getint('traitement', 'kfold')
        except ValueError:
            logger.warning('k-fold non spécifié ou incorrect (%s) => 3-fold',
                           self.process['kfold'])
            self.process['kfold'] = 3

        # choix métrique distance
        if self.process['distance'] is None or self.process['distance'] == '':
            self.process['distance'] = 'euclidean'

        #  dossiers
        self.dirs['bin'] = os.path.normpath(cfgparser.get('application',
                                                          'dossier'))
        if cfgparser.get('sortie', 'dossier').strip() == '':
            self.dirs['sortie'] = os.path.join(self.dirs['bin'], 'tmp')
        else:
            self.dirs['sortie'] = os.path.normpath(cfgparser.get('sortie',
                                                                 'dossier'))
        # get legend of classes if given in configuration file
        self.entree['codes'] = None
        if cfgparser.has_section('codes'):
            self.entree['codes'] = dict(cfgparser.items('codes'))
            for i in self.entree['codes']:
                self.entree['codes'][int(i)] = self.entree['codes'].pop(i)
            logger.info('Legend read with %i classes',
                        len(self.entree['codes']))
        cfgparser = None
        nb_pbs = self.check()
        if nb_pbs == 0:
            logger.info('Demarrage Benchmark classification...')
        else:
            logger.critical('%i Problemes detecte(s)', nb_pbs)
            raise ConfigParser.Error('Erreur de configuration')
        return nb_pbs

    def __str__(self):
        str_msg = '\n##Resume##'
        str_msg += '\n* Entree(s)\n'
        if not self.entree['echantillons'] is None:
            for i in self.entree['echantillons']:
                str_msg += '\n\t- Echantillons : %s' % i
        if not self.entree['data'] is None:
            str_msg += '\n\t- Donnees : %s' % self.entree['data']
        str_msg += '\n\n* Pretraitement'
        for i in ['scale', 'balance', 'separabilite']:
            if self.preprocess[i]:
                str_msg += '\n\t- %s' % self.__METADATA['preprocess'][i]
        if self.preprocess['reduction'] is not None:
            str_msg += '\n\t- Reduction des dimensions par %s' \
                      % self.preprocess['reduction']

        str_msg += '\n* Traitement : %s' %self.process['type']
        if self.process['algorithm'] is None:
            str_msg += '\n\t- Algorithmes : Tous'
        else:
            str_msg += '\n\t- %i algorithme(s) : %s' \
                   % (len(self.process['algorithm']), \
                   ', '.join(self.process['algorithm']))
        if self.process['optimisation']:
            str_msg += '\n\t- Optimisation des algorithmes'
        str_msg += '\n\t- Approche %i-fold' % self.process['kfold']
        str_msg += '\n\t- Distance : %s' % self.process['distance']
        str_msg += '\n* Sortie\n\t- Dossier : %s' % self.dirs['sortie']
        return str_msg
