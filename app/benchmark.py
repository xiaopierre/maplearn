# -*- coding: utf-8 -*-
"""
Classe maitresse qui compare les differentes methodes de classification
Created on Mon Dec 22 11:40:05 2014

@author: thomas_a
"""
from __future__ import print_function

import logging
import os
import numpy as np
import pandas as pd

from maplearn.ml.classification import Classification
from maplearn.ml.clustering import Clustering
from maplearn.datahandler.loader import Loader
from maplearn.datahandler.packdata import PackData
from maplearn.datahandler.writer import Writer


class Benchmark(object):
    """
    Charge et prepare les donnees puis compare les classifications
    """
    __algorithms = None
    __dctCodes = None

    def __init__(self, dossier, **kwargs):
        self.__machine = None
        self.__io = {'dossier' : dossier, 'features':None} # dossier en sortie
        if 'na' in kwargs:
            self.__io['na'] = kwargs['na']
        else:
            self.__io['na'] = np.nan
        self.dataset = None
        if 'codes' in kwargs:
            self.__dct_codes = kwargs['codes']
        else:
            self.__dct_codes = None
        
        self.__machine = eval(kwargs['type'], {"__builtins__":None}, 
                         {"classification":Classification, "clustering":Clustering})(data=None,
                         algorithms=kwargs['algorithm'], iK=2, 
                         dirOut=self.__io['dossier'], **kwargs)
        
        """        
        self.__machine = Classification(data=None,
                                        algorithms=kwargs['algorithm'],
                                      iK=2, dirOut=self.__io['dossier'],
                                      **kwargs)
        """
        self.__algorithms = self.__machine.algorithms

    def load(self, source, **kwargs):
        """
        Chargement des échantillons et des données correspondantes, plus d'un
        éventuel jeu de données complémentaire
        """
        print('##1. Chargement des donnees ##\n')
        for i in ['classe', 'classe_id', 'features']:
            if i not in kwargs:
                kwargs[i]=None
        loading = Loader(source, classe_id=kwargs['classe_id'],
                         classe=kwargs['classe'],
                         features=kwargs['features'], codes=self.__dct_codes)
        print(loading)
        source = os.path.basename(os.path.splitext(source)[0])
        self.__io['source'] = loading.src
        if self.__dct_codes is not None:
            codes = self.__dct_codes
        else:
            codes = loading.nomenclature

        if kwargs['features'] is None:
            kwargs['features'] = loading.features

        self.dataset = PackData(loading.X, loading.Y, data=loading.aData,
                                codes=codes, source=source, 
                                features=kwargs['features'],
                                na=self.__io['na'])

        if 'source' in self.__io and self.__io['source']['type'] != 'ImageGeo':
            self.__io['features'] = kwargs['features']

    def load_data(self, source, classe_id=None, classe=None, features=None):
        """
        Charge des donnees différentes des échantillons
        """
        if not features is None and not self.__io['features'] is None:
            # selection des features a utiliser (dans jeu de données)
            features = [i for i in features if i in self.__io['features']]
        elif not self.__io['features'] is None:
            features = self.__io['features']
            
        loading = Loader(source, classe_id=classe_id, classe=classe,
                         features=features, codes=self.__dct_codes)
        print(loading)
        if features is None:
            features = loading.features
        
        if 'source' in self.__io and self.__io['source']['type'] == 'ImageGeo':
            y = np.copy(self.dataset.data)
            x = np.copy(loading.matrix[y != self.__io['na']])
            y = y[y != self.__io['na']]
            self.dataset = PackData(x, y, data=loading.aData,
                                    source=source, features=features)
        else:
            self.dataset.data = loading.aData
        self.__io['data'] = loading.src

    def training(self, **kwargs):
        """
        Prepare les echantillons et le jeu de données
        """
        print('##2. Pretraitement ##\n')
        # rééchantillonne les échantillons
        if 'balance' in kwargs:
            if kwargs['balance']:
                self.dataset.balance()

        # Normalisation
        if 'scale' in kwargs:
            if kwargs['scale']:
                self.dataset.scale()

        # Reduction du nombre de dimensions du jeu de données
        if 'reduction' in kwargs and kwargs['reduction'] is not None:
            if not 'ncomp' in kwargs:
                kwargs['ncomp'] = None
            else:
                if kwargs['reduction'] != 'refcv':
                    self.dataset.reduit(meth=kwargs['reduction'],
                                        ncomp=kwargs['ncomp'])

        # analyse de separabilite
        if 'separabilite' in kwargs:
            if kwargs['separabilite']:
                if 'metric' in kwargs:
                    metric = kwargs['metric']
                else:
                    metric = 'euclidean'
                try:
                    self.dataset.separabilite(metric=metric)
                except ValueError:
                    logging.error('Analyse de separabilite impossible')
        
        # mise a dispo des donnees pour traitement
        if self.__machine.load(data=self.dataset) != 0:
            raise IOError("Erreur de chargement des données")

        if 'reduction' in kwargs and kwargs['reduction'] == 'refcv':
            self.__machine.rfe(None)

    def run(self, prediction=False, optimisation=False):
        """
        Compare les classifications
        """
        print('##3. Traitement ##\n')
        # CLASSIFICATION(S)
        # export de l'arbre de decision
        #classif.export_tree(os.path.join(dossier,'tree_rennes.dot'))
        # SELECTION DES FEATURES par RFE
        #self.__machine.rfe(self.__algorithms)

        # OPTIMISATION du classifier
        if optimisation:
            # copie des classifiers pour pouvoir ajouter
            # les classifiers optimises
            classifiers = {k:v for (k, v) in self.__algorithms.iteritems()}
            for clf in classifiers:
                self.__machine.optimize(clf)
        # comparaison des classifications
        self.__machine.run(prediction)
        
        if prediction:
            result = pd.DataFrame(data=np.zeros((len(self.dataset.not_nas),
                                                 len(self.__algorithms))),
                                                columns=self.__machine.result.columns)
            #print(self.__machine.result.describe())
            #result.ix[self.dataset.not_nas] = 1
            #print(result[result==1].describe())
            result.ix[self.dataset.not_nas] = self.__machine.result.as_matrix()
            
            # ecriture des resultats dans un fichier            
            if self.__io['data']['type'] in ['ImageGeo', ]:
                w = Writer(self.__io['data']['path'], 
                           origin=self.__io['data']['path'])
            else:
                w = Writer(self.__io['data']['path'])
            ext = os.path.splitext(self.__io['data']['path'])[-1]
            out_file = os.path.join(self.__io['dossier'], 'export' + ext)
            #if self.__io['data']['type'] == 'ImageGeo':
            
            w.run(data=result, fichier=out_file)
        logging.info('Fin du benchmark')
        return None
