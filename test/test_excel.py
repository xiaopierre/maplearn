# -*- coding: utf-8 -*-
"""
Created on Wed Aug 17 22:10:14 2016

@author: thomas_a
"""
import unittest
import os

from maplearn.test import DIR_DATA, DIR_TMP
from maplearn.filehandler.excel import Excel

class TestExcel(unittest.TestCase):
    """ Tests unitaires concernant les fichiers Excel
    """
    def test_read(self):
        """
        Lecture d'un fichier
        """
        exc = Excel(os.path.join(DIR_DATA, 'ex1.xlsx'))
        exc.read()
        test = exc.data.shape[0] == 366 and exc.data.shape[1] == 9
        if not test:
            print(exc.data.shape)
        self.assertTrue(test)

    def test_write(self):
        """
        Ecriture d'un fichier
        """
        exc = Excel(os.path.join(DIR_DATA, 'ex1.xlsx'))
        exc.read()
        out_file = os.path.join(DIR_TMP, 'ex1.xlsx')
        exc.write(out_file)
        self.assertTrue(os.path.exists(out_file))

if __name__ == '__main__':
    unittest.main()