# -*- coding: utf-8 -*-
"""
Created on Tue Aug 16 18:37:27 2016

@author: thomas_a
"""
import os
import logging

from maplearn.app.config import Config

cfg = Config('config.cfg')
DIR_DATA = os.path.join(cfg.dirs['bin'], 'datasets')
DIR_TMP = os.path.join(cfg.dirs['bin'], 'tmp')

logging.config.fileConfig(os.path.join(cfg.dirs['bin'], 'logging.cfg'),
                          disable_existing_loggers=False)
logging.getLogger('maplearn')

if not os.path.exists(DIR_TMP):
    os.mkdir(DIR_TMP)
