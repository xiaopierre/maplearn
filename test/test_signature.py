# -*- coding: utf-8 -*-
"""
Created on Wed Aug 17 22:17:10 2016

@author: thomas_a
"""
import unittest

from maplearn.datahandler.loader import Loader
from maplearn.datahandler.signature import Signature

class TestSignature(unittest.TestCase):
    """ Tests unitaires de la classe Signature
    """
    def setUp(self):
        loader = Loader('iris')
        self.__data = loader.X
        self.__features = ['Sepal length', 'Sepal width', 'Petal length',
                           'Petal width']

    def test_boxplot(self):
        """
        Graphique Boxplot : vérifie que tout se passe bien
        """
        sig = Signature()
        self.assertRaises(None, sig.boxplot(self.__data,
                                            features=self.__features))

    def test_plot(self):
        """
        plot : vérifie que tout se passe bien
        """
        sig = Signature()
        self.assertRaises(None, sig.plot(self.__data, title='test'))

if __name__ == '__main__':
    unittest.main()